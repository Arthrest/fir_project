
from myhdl import Signal, intbv, always, block, delay, instance, StopSimulation
from scipy import signal
from FIR_python import FIR_Python as myFIR


@block
def FIR(clock, sig_in, sig_out, coef):
    """
    """
    # tworzenie tablicy 0 o rozmiarze równym ilości współczynników filtru
    tab = [Signal(intbv(0, min=sig_in.min, max=sig_in.max))
            for ii in range(len(coef))]
    # convert coef from list to tuple
    coef = tuple(coef)
    bit_shift = len(sig_in) - 1

    # wykonaj przy każdym narastającym zboczu zegara
    @always(clock.posedge)
    def filtrate():
            result = 0
            # Note this adds an extra delay! (Group delay N/2+1)
            # pętla po wszystkich współczynnikach filtru wynika ze schematu filtru fir, gdzie wszystkie współczynniki się sumują (poprzesuwane w czasie)
            for ii in range(len(coef)):
                if ii == 0:
                    tab[ii].next = sig_in
                else:
                    tab[ii].next = tab[ii - 1]
                c = coef[ii]
                result = result + (tab[ii] * c)
            # przepisanie na wyjście, robi się przesunięcie bitowe o 15, czyli dzielenie przez 2^15 - po co? pewnie, żeby przeskalować i się zmieścić w rozdzielczości sygnału, który ma tylko 16 bitów
            sig_out.next = (result >> bit_shift)

    return filtrate


@block
def TEST():
    HALF_PERIOD = delay(10)

    clock = Signal(bool(0))

    # length of filter (number of coefficients) - liczba współczynników filtru
    N = 2
    # cutoff frequency of filter
    cut_off = 0.5

    # word size - ilo bitowy będzie sygnał
    signalMax = 2 ** 15
    signalMin = -1 * signalMax

    #Create filter
    our_fir = myFIR(N, cut_off)
    # our_fir.plot_filter()

    # initial value of signals input and output (both 0)
    sig_in = Signal(intbv(0, min=signalMin, max=signalMax))
    sig_out = Signal(intbv(0, min=signalMin, max=signalMax))

    rounded_coef = list(map(int, our_fir.coefficients * signalMax))

    fir_filter = FIR(clock, sig_in, sig_out, rounded_coef)

    @always(HALF_PERIOD)
    def clockGen():
        clock.next = not clock

    @instance
    def stimulus():
        yield clock.negedge

        tab = [24, 27, 31, 59, 33, 37, 0, 0, 0, 0, 0, 0]

        print("sigin       sigout     ")
        for point in tab:
            sig_in.next = Signal(intbv(point, min=signalMin, max=signalMax))
            print("  %d         %d" % (sig_in, sig_out))
            yield clock.negedge
        raise StopSimulation()

    return clockGen, stimulus, fir_filter

tb = TEST()
tb.run_sim()