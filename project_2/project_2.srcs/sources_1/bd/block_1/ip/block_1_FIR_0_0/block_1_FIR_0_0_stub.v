// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Mon Dec  3 15:14:46 2018
// Host        : PC05 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/student/Documents/AK/FIR/fir_project/project_2/project_2.srcs/sources_1/bd/block_1/ip/block_1_FIR_0_0/block_1_FIR_0_0_stub.v
// Design      : block_1_FIR_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "FIR,Vivado 2018.2" *)
module block_1_FIR_0_0(clock, axis_s_raw_tdata, axis_s_raw_tready, 
  axis_s_raw_tvalid, axis_s_raw_tlast, axis_m_filtr_tdata, axis_m_filtr_tready, 
  axis_m_filtr_tvalid, axis_m_filtr_tlast)
/* synthesis syn_black_box black_box_pad_pin="clock,axis_s_raw_tdata[31:0],axis_s_raw_tready,axis_s_raw_tvalid,axis_s_raw_tlast,axis_m_filtr_tdata[31:0],axis_m_filtr_tready,axis_m_filtr_tvalid,axis_m_filtr_tlast" */;
  input clock;
  input [31:0]axis_s_raw_tdata;
  output axis_s_raw_tready;
  input axis_s_raw_tvalid;
  input axis_s_raw_tlast;
  output [31:0]axis_m_filtr_tdata;
  input axis_m_filtr_tready;
  output axis_m_filtr_tvalid;
  output axis_m_filtr_tlast;
endmodule
