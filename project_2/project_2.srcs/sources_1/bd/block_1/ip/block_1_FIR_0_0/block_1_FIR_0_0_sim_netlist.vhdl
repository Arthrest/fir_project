-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
-- Date        : Mon Dec  3 15:14:46 2018
-- Host        : PC05 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/student/Documents/AK/FIR/fir_project/project_2/project_2.srcs/sources_1/bd/block_1/ip/block_1_FIR_0_0/block_1_FIR_0_0_sim_netlist.vhdl
-- Design      : block_1_FIR_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity block_1_FIR_0_0_FIR is
  port (
    axis_s_raw_tready : out STD_LOGIC;
    axis_m_filtr_tdata : out STD_LOGIC_VECTOR ( 16 downto 0 );
    axis_m_filtr_tvalid : out STD_LOGIC;
    axis_m_filtr_tlast : out STD_LOGIC;
    axis_m_filtr_tready : in STD_LOGIC;
    clock : in STD_LOGIC;
    axis_s_raw_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    axis_s_raw_tvalid : in STD_LOGIC;
    axis_s_raw_tlast : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of block_1_FIR_0_0_FIR : entity is "FIR";
end block_1_FIR_0_0_FIR;

architecture STRUCTURE of block_1_FIR_0_0_FIR is
  signal SHIFT_RIGHT0 : STD_LOGIC;
  signal \result[13]_i_2_n_0\ : STD_LOGIC;
  signal \result[13]_i_3_n_0\ : STD_LOGIC;
  signal \result[13]_i_4_n_0\ : STD_LOGIC;
  signal \result[17]_i_2_n_0\ : STD_LOGIC;
  signal \result[17]_i_3_n_0\ : STD_LOGIC;
  signal \result[17]_i_4_n_0\ : STD_LOGIC;
  signal \result[17]_i_5_n_0\ : STD_LOGIC;
  signal \result[21]_i_2_n_0\ : STD_LOGIC;
  signal \result[21]_i_3_n_0\ : STD_LOGIC;
  signal \result[21]_i_4_n_0\ : STD_LOGIC;
  signal \result[21]_i_5_n_0\ : STD_LOGIC;
  signal \result[25]_i_2_n_0\ : STD_LOGIC;
  signal \result[25]_i_3_n_0\ : STD_LOGIC;
  signal \result[25]_i_4_n_0\ : STD_LOGIC;
  signal \result[25]_i_5_n_0\ : STD_LOGIC;
  signal \result[29]_i_2_n_0\ : STD_LOGIC;
  signal \result[29]_i_3_n_0\ : STD_LOGIC;
  signal \result[29]_i_4_n_0\ : STD_LOGIC;
  signal result_final0 : STD_LOGIC_VECTOR ( 31 downto 15 );
  signal \result_final0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \result_final0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \result_final0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \result_final0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \result_final0_carry__0_n_0\ : STD_LOGIC;
  signal \result_final0_carry__0_n_1\ : STD_LOGIC;
  signal \result_final0_carry__0_n_2\ : STD_LOGIC;
  signal \result_final0_carry__0_n_3\ : STD_LOGIC;
  signal \result_final0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \result_final0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \result_final0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \result_final0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \result_final0_carry__1_n_0\ : STD_LOGIC;
  signal \result_final0_carry__1_n_1\ : STD_LOGIC;
  signal \result_final0_carry__1_n_2\ : STD_LOGIC;
  signal \result_final0_carry__1_n_3\ : STD_LOGIC;
  signal \result_final0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \result_final0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \result_final0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \result_final0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \result_final0_carry__2_n_0\ : STD_LOGIC;
  signal \result_final0_carry__2_n_1\ : STD_LOGIC;
  signal \result_final0_carry__2_n_2\ : STD_LOGIC;
  signal \result_final0_carry__2_n_3\ : STD_LOGIC;
  signal \result_final0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \result_final0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \result_final0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \result_final0_carry__3_n_2\ : STD_LOGIC;
  signal \result_final0_carry__3_n_3\ : STD_LOGIC;
  signal result_final0_carry_i_1_n_0 : STD_LOGIC;
  signal result_final0_carry_i_2_n_0 : STD_LOGIC;
  signal result_final0_carry_i_3_n_0 : STD_LOGIC;
  signal result_final0_carry_i_4_n_0 : STD_LOGIC;
  signal result_final0_carry_n_0 : STD_LOGIC;
  signal result_final0_carry_n_1 : STD_LOGIC;
  signal result_final0_carry_n_2 : STD_LOGIC;
  signal result_final0_carry_n_3 : STD_LOGIC;
  signal \result_final_reg_n_0_[15]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[16]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[17]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[18]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[19]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[20]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[21]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[22]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[23]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[24]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[25]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[26]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[27]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[28]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[29]\ : STD_LOGIC;
  signal \result_final_reg_n_0_[30]\ : STD_LOGIC;
  signal result_guard : STD_LOGIC_VECTOR ( 31 downto 13 );
  signal result_reg : STD_LOGIC_VECTOR ( 31 downto 13 );
  signal \result_reg[13]_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[13]_i_1_n_1\ : STD_LOGIC;
  signal \result_reg[13]_i_1_n_2\ : STD_LOGIC;
  signal \result_reg[13]_i_1_n_3\ : STD_LOGIC;
  signal \result_reg[13]_i_1_n_4\ : STD_LOGIC;
  signal \result_reg[13]_i_1_n_5\ : STD_LOGIC;
  signal \result_reg[13]_i_1_n_6\ : STD_LOGIC;
  signal \result_reg[13]_i_1_n_7\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_1\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_2\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_3\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_4\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_5\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_6\ : STD_LOGIC;
  signal \result_reg[17]_i_1_n_7\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_1\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_2\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_3\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_4\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_5\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_6\ : STD_LOGIC;
  signal \result_reg[21]_i_1_n_7\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_0\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_1\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_2\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_3\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_4\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_5\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_6\ : STD_LOGIC;
  signal \result_reg[25]_i_1_n_7\ : STD_LOGIC;
  signal \result_reg[29]_i_1_n_2\ : STD_LOGIC;
  signal \result_reg[29]_i_1_n_3\ : STD_LOGIC;
  signal \result_reg[29]_i_1_n_5\ : STD_LOGIC;
  signal \result_reg[29]_i_1_n_6\ : STD_LOGIC;
  signal \result_reg[29]_i_1_n_7\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][0]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][10]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][11]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][12]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][13]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][14]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][15]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][1]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][2]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][3]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][4]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][5]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][6]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][7]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][8]\ : STD_LOGIC;
  signal \tab_reg_n_0_[0][9]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][0]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][10]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][11]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][12]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][13]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][14]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][15]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][1]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][2]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][3]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][4]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][5]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][6]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][7]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][8]\ : STD_LOGIC;
  signal \tab_reg_n_0_[1][9]\ : STD_LOGIC;
  signal NLW_result_final0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_result_final0_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_result_final0_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_result_reg[29]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_result_reg[29]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\axis_m_filtr_tdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[15]\,
      Q => axis_m_filtr_tdata(0),
      R => '0'
    );
\axis_m_filtr_tdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[25]\,
      Q => axis_m_filtr_tdata(10),
      R => '0'
    );
\axis_m_filtr_tdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[26]\,
      Q => axis_m_filtr_tdata(11),
      R => '0'
    );
\axis_m_filtr_tdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[27]\,
      Q => axis_m_filtr_tdata(12),
      R => '0'
    );
\axis_m_filtr_tdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[28]\,
      Q => axis_m_filtr_tdata(13),
      R => '0'
    );
\axis_m_filtr_tdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[29]\,
      Q => axis_m_filtr_tdata(14),
      R => '0'
    );
\axis_m_filtr_tdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[30]\,
      Q => axis_m_filtr_tdata(15),
      R => '0'
    );
\axis_m_filtr_tdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[16]\,
      Q => axis_m_filtr_tdata(1),
      R => '0'
    );
\axis_m_filtr_tdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[17]\,
      Q => axis_m_filtr_tdata(2),
      R => '0'
    );
\axis_m_filtr_tdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => SHIFT_RIGHT0,
      Q => axis_m_filtr_tdata(16),
      R => '0'
    );
\axis_m_filtr_tdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[18]\,
      Q => axis_m_filtr_tdata(3),
      R => '0'
    );
\axis_m_filtr_tdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[19]\,
      Q => axis_m_filtr_tdata(4),
      R => '0'
    );
\axis_m_filtr_tdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[20]\,
      Q => axis_m_filtr_tdata(5),
      R => '0'
    );
\axis_m_filtr_tdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[21]\,
      Q => axis_m_filtr_tdata(6),
      R => '0'
    );
\axis_m_filtr_tdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[22]\,
      Q => axis_m_filtr_tdata(7),
      R => '0'
    );
\axis_m_filtr_tdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[23]\,
      Q => axis_m_filtr_tdata(8),
      R => '0'
    );
\axis_m_filtr_tdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_final_reg_n_0_[24]\,
      Q => axis_m_filtr_tdata(9),
      R => '0'
    );
axis_m_filtr_tlast_reg: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tlast,
      Q => axis_m_filtr_tlast,
      R => '0'
    );
axis_m_filtr_tvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tvalid,
      Q => axis_m_filtr_tvalid,
      R => '0'
    );
axis_s_raw_tready_reg: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_m_filtr_tready,
      Q => axis_s_raw_tready,
      R => '0'
    );
\result[13]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][2]\,
      I1 => result_reg(16),
      O => \result[13]_i_2_n_0\
    );
\result[13]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][1]\,
      I1 => result_reg(15),
      O => \result[13]_i_3_n_0\
    );
\result[13]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][0]\,
      I1 => result_reg(14),
      O => \result[13]_i_4_n_0\
    );
\result[17]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][6]\,
      I1 => result_reg(20),
      O => \result[17]_i_2_n_0\
    );
\result[17]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][5]\,
      I1 => result_reg(19),
      O => \result[17]_i_3_n_0\
    );
\result[17]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][4]\,
      I1 => result_reg(18),
      O => \result[17]_i_4_n_0\
    );
\result[17]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][3]\,
      I1 => result_reg(17),
      O => \result[17]_i_5_n_0\
    );
\result[21]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][10]\,
      I1 => result_reg(24),
      O => \result[21]_i_2_n_0\
    );
\result[21]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][9]\,
      I1 => result_reg(23),
      O => \result[21]_i_3_n_0\
    );
\result[21]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][8]\,
      I1 => result_reg(22),
      O => \result[21]_i_4_n_0\
    );
\result[21]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][7]\,
      I1 => result_reg(21),
      O => \result[21]_i_5_n_0\
    );
\result[25]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][14]\,
      I1 => result_reg(28),
      O => \result[25]_i_2_n_0\
    );
\result[25]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][13]\,
      I1 => result_reg(27),
      O => \result[25]_i_3_n_0\
    );
\result[25]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][12]\,
      I1 => result_reg(26),
      O => \result[25]_i_4_n_0\
    );
\result[25]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][11]\,
      I1 => result_reg(25),
      O => \result[25]_i_5_n_0\
    );
\result[29]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][15]\,
      I1 => result_reg(31),
      O => \result[29]_i_2_n_0\
    );
\result[29]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][15]\,
      I1 => result_reg(30),
      O => \result[29]_i_3_n_0\
    );
\result[29]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tab_reg_n_0_[1][15]\,
      I1 => result_reg(29),
      O => \result[29]_i_4_n_0\
    );
result_final0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => result_final0_carry_n_0,
      CO(2) => result_final0_carry_n_1,
      CO(1) => result_final0_carry_n_2,
      CO(0) => result_final0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => result_reg(16 downto 13),
      O(3 downto 2) => result_final0(16 downto 15),
      O(1 downto 0) => NLW_result_final0_carry_O_UNCONNECTED(1 downto 0),
      S(3) => result_final0_carry_i_1_n_0,
      S(2) => result_final0_carry_i_2_n_0,
      S(1) => result_final0_carry_i_3_n_0,
      S(0) => result_final0_carry_i_4_n_0
    );
\result_final0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => result_final0_carry_n_0,
      CO(3) => \result_final0_carry__0_n_0\,
      CO(2) => \result_final0_carry__0_n_1\,
      CO(1) => \result_final0_carry__0_n_2\,
      CO(0) => \result_final0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => result_reg(20 downto 17),
      O(3 downto 0) => result_final0(20 downto 17),
      S(3) => \result_final0_carry__0_i_1_n_0\,
      S(2) => \result_final0_carry__0_i_2_n_0\,
      S(1) => \result_final0_carry__0_i_3_n_0\,
      S(0) => \result_final0_carry__0_i_4_n_0\
    );
\result_final0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(20),
      I1 => result_guard(20),
      O => \result_final0_carry__0_i_1_n_0\
    );
\result_final0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(19),
      I1 => result_guard(19),
      O => \result_final0_carry__0_i_2_n_0\
    );
\result_final0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(18),
      I1 => result_guard(18),
      O => \result_final0_carry__0_i_3_n_0\
    );
\result_final0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(17),
      I1 => result_guard(17),
      O => \result_final0_carry__0_i_4_n_0\
    );
\result_final0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_final0_carry__0_n_0\,
      CO(3) => \result_final0_carry__1_n_0\,
      CO(2) => \result_final0_carry__1_n_1\,
      CO(1) => \result_final0_carry__1_n_2\,
      CO(0) => \result_final0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => result_reg(24 downto 21),
      O(3 downto 0) => result_final0(24 downto 21),
      S(3) => \result_final0_carry__1_i_1_n_0\,
      S(2) => \result_final0_carry__1_i_2_n_0\,
      S(1) => \result_final0_carry__1_i_3_n_0\,
      S(0) => \result_final0_carry__1_i_4_n_0\
    );
\result_final0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(24),
      I1 => result_guard(24),
      O => \result_final0_carry__1_i_1_n_0\
    );
\result_final0_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(23),
      I1 => result_guard(23),
      O => \result_final0_carry__1_i_2_n_0\
    );
\result_final0_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(22),
      I1 => result_guard(22),
      O => \result_final0_carry__1_i_3_n_0\
    );
\result_final0_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(21),
      I1 => result_guard(21),
      O => \result_final0_carry__1_i_4_n_0\
    );
\result_final0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_final0_carry__1_n_0\,
      CO(3) => \result_final0_carry__2_n_0\,
      CO(2) => \result_final0_carry__2_n_1\,
      CO(1) => \result_final0_carry__2_n_2\,
      CO(0) => \result_final0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => result_reg(28 downto 25),
      O(3 downto 0) => result_final0(28 downto 25),
      S(3) => \result_final0_carry__2_i_1_n_0\,
      S(2) => \result_final0_carry__2_i_2_n_0\,
      S(1) => \result_final0_carry__2_i_3_n_0\,
      S(0) => \result_final0_carry__2_i_4_n_0\
    );
\result_final0_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(28),
      I1 => result_guard(28),
      O => \result_final0_carry__2_i_1_n_0\
    );
\result_final0_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(27),
      I1 => result_guard(27),
      O => \result_final0_carry__2_i_2_n_0\
    );
\result_final0_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(26),
      I1 => result_guard(26),
      O => \result_final0_carry__2_i_3_n_0\
    );
\result_final0_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(25),
      I1 => result_guard(25),
      O => \result_final0_carry__2_i_4_n_0\
    );
\result_final0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_final0_carry__2_n_0\,
      CO(3 downto 2) => \NLW_result_final0_carry__3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \result_final0_carry__3_n_2\,
      CO(0) => \result_final0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => result_reg(30 downto 29),
      O(3) => \NLW_result_final0_carry__3_O_UNCONNECTED\(3),
      O(2 downto 0) => result_final0(31 downto 29),
      S(3) => '0',
      S(2) => \result_final0_carry__3_i_1_n_0\,
      S(1) => \result_final0_carry__3_i_2_n_0\,
      S(0) => \result_final0_carry__3_i_3_n_0\
    );
\result_final0_carry__3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(31),
      I1 => result_guard(31),
      O => \result_final0_carry__3_i_1_n_0\
    );
\result_final0_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(30),
      I1 => result_guard(30),
      O => \result_final0_carry__3_i_2_n_0\
    );
\result_final0_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(29),
      I1 => result_guard(29),
      O => \result_final0_carry__3_i_3_n_0\
    );
result_final0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(16),
      I1 => result_guard(16),
      O => result_final0_carry_i_1_n_0
    );
result_final0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(15),
      I1 => result_guard(15),
      O => result_final0_carry_i_2_n_0
    );
result_final0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(14),
      I1 => result_guard(14),
      O => result_final0_carry_i_3_n_0
    );
result_final0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => result_reg(13),
      I1 => result_guard(13),
      O => result_final0_carry_i_4_n_0
    );
\result_final_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(15),
      Q => \result_final_reg_n_0_[15]\,
      R => '0'
    );
\result_final_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(16),
      Q => \result_final_reg_n_0_[16]\,
      R => '0'
    );
\result_final_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(17),
      Q => \result_final_reg_n_0_[17]\,
      R => '0'
    );
\result_final_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(18),
      Q => \result_final_reg_n_0_[18]\,
      R => '0'
    );
\result_final_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(19),
      Q => \result_final_reg_n_0_[19]\,
      R => '0'
    );
\result_final_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(20),
      Q => \result_final_reg_n_0_[20]\,
      R => '0'
    );
\result_final_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(21),
      Q => \result_final_reg_n_0_[21]\,
      R => '0'
    );
\result_final_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(22),
      Q => \result_final_reg_n_0_[22]\,
      R => '0'
    );
\result_final_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(23),
      Q => \result_final_reg_n_0_[23]\,
      R => '0'
    );
\result_final_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(24),
      Q => \result_final_reg_n_0_[24]\,
      R => '0'
    );
\result_final_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(25),
      Q => \result_final_reg_n_0_[25]\,
      R => '0'
    );
\result_final_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(26),
      Q => \result_final_reg_n_0_[26]\,
      R => '0'
    );
\result_final_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(27),
      Q => \result_final_reg_n_0_[27]\,
      R => '0'
    );
\result_final_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(28),
      Q => \result_final_reg_n_0_[28]\,
      R => '0'
    );
\result_final_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(29),
      Q => \result_final_reg_n_0_[29]\,
      R => '0'
    );
\result_final_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(30),
      Q => \result_final_reg_n_0_[30]\,
      R => '0'
    );
\result_final_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_final0(31),
      Q => SHIFT_RIGHT0,
      R => '0'
    );
\result_guard_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(13),
      Q => result_guard(13),
      R => '0'
    );
\result_guard_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(14),
      Q => result_guard(14),
      R => '0'
    );
\result_guard_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(15),
      Q => result_guard(15),
      R => '0'
    );
\result_guard_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(16),
      Q => result_guard(16),
      R => '0'
    );
\result_guard_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(17),
      Q => result_guard(17),
      R => '0'
    );
\result_guard_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(18),
      Q => result_guard(18),
      R => '0'
    );
\result_guard_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(19),
      Q => result_guard(19),
      R => '0'
    );
\result_guard_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(20),
      Q => result_guard(20),
      R => '0'
    );
\result_guard_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(21),
      Q => result_guard(21),
      R => '0'
    );
\result_guard_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(22),
      Q => result_guard(22),
      R => '0'
    );
\result_guard_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(23),
      Q => result_guard(23),
      R => '0'
    );
\result_guard_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(24),
      Q => result_guard(24),
      R => '0'
    );
\result_guard_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(25),
      Q => result_guard(25),
      R => '0'
    );
\result_guard_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(26),
      Q => result_guard(26),
      R => '0'
    );
\result_guard_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(27),
      Q => result_guard(27),
      R => '0'
    );
\result_guard_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(28),
      Q => result_guard(28),
      R => '0'
    );
\result_guard_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(29),
      Q => result_guard(29),
      R => '0'
    );
\result_guard_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(30),
      Q => result_guard(30),
      R => '0'
    );
\result_guard_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => result_reg(31),
      Q => result_guard(31),
      R => '0'
    );
\result_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[13]_i_1_n_7\,
      Q => result_reg(13),
      R => '0'
    );
\result_reg[13]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \result_reg[13]_i_1_n_0\,
      CO(2) => \result_reg[13]_i_1_n_1\,
      CO(1) => \result_reg[13]_i_1_n_2\,
      CO(0) => \result_reg[13]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \tab_reg_n_0_[1][2]\,
      DI(2) => \tab_reg_n_0_[1][1]\,
      DI(1) => \tab_reg_n_0_[1][0]\,
      DI(0) => '0',
      O(3) => \result_reg[13]_i_1_n_4\,
      O(2) => \result_reg[13]_i_1_n_5\,
      O(1) => \result_reg[13]_i_1_n_6\,
      O(0) => \result_reg[13]_i_1_n_7\,
      S(3) => \result[13]_i_2_n_0\,
      S(2) => \result[13]_i_3_n_0\,
      S(1) => \result[13]_i_4_n_0\,
      S(0) => result_reg(13)
    );
\result_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[13]_i_1_n_6\,
      Q => result_reg(14),
      R => '0'
    );
\result_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[13]_i_1_n_5\,
      Q => result_reg(15),
      R => '0'
    );
\result_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[13]_i_1_n_4\,
      Q => result_reg(16),
      R => '0'
    );
\result_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[17]_i_1_n_7\,
      Q => result_reg(17),
      R => '0'
    );
\result_reg[17]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_reg[13]_i_1_n_0\,
      CO(3) => \result_reg[17]_i_1_n_0\,
      CO(2) => \result_reg[17]_i_1_n_1\,
      CO(1) => \result_reg[17]_i_1_n_2\,
      CO(0) => \result_reg[17]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \tab_reg_n_0_[1][6]\,
      DI(2) => \tab_reg_n_0_[1][5]\,
      DI(1) => \tab_reg_n_0_[1][4]\,
      DI(0) => \tab_reg_n_0_[1][3]\,
      O(3) => \result_reg[17]_i_1_n_4\,
      O(2) => \result_reg[17]_i_1_n_5\,
      O(1) => \result_reg[17]_i_1_n_6\,
      O(0) => \result_reg[17]_i_1_n_7\,
      S(3) => \result[17]_i_2_n_0\,
      S(2) => \result[17]_i_3_n_0\,
      S(1) => \result[17]_i_4_n_0\,
      S(0) => \result[17]_i_5_n_0\
    );
\result_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[17]_i_1_n_6\,
      Q => result_reg(18),
      R => '0'
    );
\result_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[17]_i_1_n_5\,
      Q => result_reg(19),
      R => '0'
    );
\result_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[17]_i_1_n_4\,
      Q => result_reg(20),
      R => '0'
    );
\result_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[21]_i_1_n_7\,
      Q => result_reg(21),
      R => '0'
    );
\result_reg[21]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_reg[17]_i_1_n_0\,
      CO(3) => \result_reg[21]_i_1_n_0\,
      CO(2) => \result_reg[21]_i_1_n_1\,
      CO(1) => \result_reg[21]_i_1_n_2\,
      CO(0) => \result_reg[21]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \tab_reg_n_0_[1][10]\,
      DI(2) => \tab_reg_n_0_[1][9]\,
      DI(1) => \tab_reg_n_0_[1][8]\,
      DI(0) => \tab_reg_n_0_[1][7]\,
      O(3) => \result_reg[21]_i_1_n_4\,
      O(2) => \result_reg[21]_i_1_n_5\,
      O(1) => \result_reg[21]_i_1_n_6\,
      O(0) => \result_reg[21]_i_1_n_7\,
      S(3) => \result[21]_i_2_n_0\,
      S(2) => \result[21]_i_3_n_0\,
      S(1) => \result[21]_i_4_n_0\,
      S(0) => \result[21]_i_5_n_0\
    );
\result_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[21]_i_1_n_6\,
      Q => result_reg(22),
      R => '0'
    );
\result_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[21]_i_1_n_5\,
      Q => result_reg(23),
      R => '0'
    );
\result_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[21]_i_1_n_4\,
      Q => result_reg(24),
      R => '0'
    );
\result_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[25]_i_1_n_7\,
      Q => result_reg(25),
      R => '0'
    );
\result_reg[25]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_reg[21]_i_1_n_0\,
      CO(3) => \result_reg[25]_i_1_n_0\,
      CO(2) => \result_reg[25]_i_1_n_1\,
      CO(1) => \result_reg[25]_i_1_n_2\,
      CO(0) => \result_reg[25]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \tab_reg_n_0_[1][14]\,
      DI(2) => \tab_reg_n_0_[1][13]\,
      DI(1) => \tab_reg_n_0_[1][12]\,
      DI(0) => \tab_reg_n_0_[1][11]\,
      O(3) => \result_reg[25]_i_1_n_4\,
      O(2) => \result_reg[25]_i_1_n_5\,
      O(1) => \result_reg[25]_i_1_n_6\,
      O(0) => \result_reg[25]_i_1_n_7\,
      S(3) => \result[25]_i_2_n_0\,
      S(2) => \result[25]_i_3_n_0\,
      S(1) => \result[25]_i_4_n_0\,
      S(0) => \result[25]_i_5_n_0\
    );
\result_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[25]_i_1_n_6\,
      Q => result_reg(26),
      R => '0'
    );
\result_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[25]_i_1_n_5\,
      Q => result_reg(27),
      R => '0'
    );
\result_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[25]_i_1_n_4\,
      Q => result_reg(28),
      R => '0'
    );
\result_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[29]_i_1_n_7\,
      Q => result_reg(29),
      R => '0'
    );
\result_reg[29]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \result_reg[25]_i_1_n_0\,
      CO(3 downto 2) => \NLW_result_reg[29]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \result_reg[29]_i_1_n_2\,
      CO(0) => \result_reg[29]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \tab_reg_n_0_[1][15]\,
      DI(0) => \tab_reg_n_0_[1][15]\,
      O(3) => \NLW_result_reg[29]_i_1_O_UNCONNECTED\(3),
      O(2) => \result_reg[29]_i_1_n_5\,
      O(1) => \result_reg[29]_i_1_n_6\,
      O(0) => \result_reg[29]_i_1_n_7\,
      S(3) => '0',
      S(2) => \result[29]_i_2_n_0\,
      S(1) => \result[29]_i_3_n_0\,
      S(0) => \result[29]_i_4_n_0\
    );
\result_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[29]_i_1_n_6\,
      Q => result_reg(30),
      R => '0'
    );
\result_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \result_reg[29]_i_1_n_5\,
      Q => result_reg(31),
      R => '0'
    );
\tab_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(0),
      Q => \tab_reg_n_0_[0][0]\,
      R => '0'
    );
\tab_reg[0][10]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(10),
      Q => \tab_reg_n_0_[0][10]\,
      R => '0'
    );
\tab_reg[0][11]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(11),
      Q => \tab_reg_n_0_[0][11]\,
      R => '0'
    );
\tab_reg[0][12]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(12),
      Q => \tab_reg_n_0_[0][12]\,
      R => '0'
    );
\tab_reg[0][13]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(13),
      Q => \tab_reg_n_0_[0][13]\,
      R => '0'
    );
\tab_reg[0][14]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(14),
      Q => \tab_reg_n_0_[0][14]\,
      R => '0'
    );
\tab_reg[0][15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(15),
      Q => \tab_reg_n_0_[0][15]\,
      R => '0'
    );
\tab_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(1),
      Q => \tab_reg_n_0_[0][1]\,
      R => '0'
    );
\tab_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(2),
      Q => \tab_reg_n_0_[0][2]\,
      R => '0'
    );
\tab_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(3),
      Q => \tab_reg_n_0_[0][3]\,
      R => '0'
    );
\tab_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(4),
      Q => \tab_reg_n_0_[0][4]\,
      R => '0'
    );
\tab_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(5),
      Q => \tab_reg_n_0_[0][5]\,
      R => '0'
    );
\tab_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(6),
      Q => \tab_reg_n_0_[0][6]\,
      R => '0'
    );
\tab_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(7),
      Q => \tab_reg_n_0_[0][7]\,
      R => '0'
    );
\tab_reg[0][8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(8),
      Q => \tab_reg_n_0_[0][8]\,
      R => '0'
    );
\tab_reg[0][9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => axis_s_raw_tdata(9),
      Q => \tab_reg_n_0_[0][9]\,
      R => '0'
    );
\tab_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][0]\,
      Q => \tab_reg_n_0_[1][0]\,
      R => '0'
    );
\tab_reg[1][10]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][10]\,
      Q => \tab_reg_n_0_[1][10]\,
      R => '0'
    );
\tab_reg[1][11]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][11]\,
      Q => \tab_reg_n_0_[1][11]\,
      R => '0'
    );
\tab_reg[1][12]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][12]\,
      Q => \tab_reg_n_0_[1][12]\,
      R => '0'
    );
\tab_reg[1][13]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][13]\,
      Q => \tab_reg_n_0_[1][13]\,
      R => '0'
    );
\tab_reg[1][14]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][14]\,
      Q => \tab_reg_n_0_[1][14]\,
      R => '0'
    );
\tab_reg[1][15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][15]\,
      Q => \tab_reg_n_0_[1][15]\,
      R => '0'
    );
\tab_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][1]\,
      Q => \tab_reg_n_0_[1][1]\,
      R => '0'
    );
\tab_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][2]\,
      Q => \tab_reg_n_0_[1][2]\,
      R => '0'
    );
\tab_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][3]\,
      Q => \tab_reg_n_0_[1][3]\,
      R => '0'
    );
\tab_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][4]\,
      Q => \tab_reg_n_0_[1][4]\,
      R => '0'
    );
\tab_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][5]\,
      Q => \tab_reg_n_0_[1][5]\,
      R => '0'
    );
\tab_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][6]\,
      Q => \tab_reg_n_0_[1][6]\,
      R => '0'
    );
\tab_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][7]\,
      Q => \tab_reg_n_0_[1][7]\,
      R => '0'
    );
\tab_reg[1][8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][8]\,
      Q => \tab_reg_n_0_[1][8]\,
      R => '0'
    );
\tab_reg[1][9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => '1',
      D => \tab_reg_n_0_[0][9]\,
      Q => \tab_reg_n_0_[1][9]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity block_1_FIR_0_0 is
  port (
    clock : in STD_LOGIC;
    axis_s_raw_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axis_s_raw_tready : out STD_LOGIC;
    axis_s_raw_tvalid : in STD_LOGIC;
    axis_s_raw_tlast : in STD_LOGIC;
    axis_m_filtr_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axis_m_filtr_tready : in STD_LOGIC;
    axis_m_filtr_tvalid : out STD_LOGIC;
    axis_m_filtr_tlast : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of block_1_FIR_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of block_1_FIR_0_0 : entity is "block_1_FIR_0_0,FIR,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of block_1_FIR_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of block_1_FIR_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of block_1_FIR_0_0 : entity is "FIR,Vivado 2018.2";
end block_1_FIR_0_0;

architecture STRUCTURE of block_1_FIR_0_0 is
  signal \^axis_m_filtr_tdata\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of axis_m_filtr_tlast : signal is "xilinx.com:interface:axis:1.0 axis_m_filtr TLAST";
  attribute x_interface_info of axis_m_filtr_tready : signal is "xilinx.com:interface:axis:1.0 axis_m_filtr TREADY";
  attribute x_interface_info of axis_m_filtr_tvalid : signal is "xilinx.com:interface:axis:1.0 axis_m_filtr TVALID";
  attribute x_interface_info of axis_s_raw_tlast : signal is "xilinx.com:interface:axis:1.0 axis_s_raw TLAST";
  attribute x_interface_info of axis_s_raw_tready : signal is "xilinx.com:interface:axis:1.0 axis_s_raw TREADY";
  attribute x_interface_info of axis_s_raw_tvalid : signal is "xilinx.com:interface:axis:1.0 axis_s_raw TVALID";
  attribute x_interface_info of clock : signal is "xilinx.com:signal:clock:1.0 clock CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of clock : signal is "XIL_INTERFACENAME clock, ASSOCIATED_BUSIF axis_m_filtr:axis_s_raw, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_1_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of axis_m_filtr_tdata : signal is "xilinx.com:interface:axis:1.0 axis_m_filtr TDATA";
  attribute x_interface_parameter of axis_m_filtr_tdata : signal is "XIL_INTERFACENAME axis_m_filtr, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  attribute x_interface_info of axis_s_raw_tdata : signal is "xilinx.com:interface:axis:1.0 axis_s_raw TDATA";
  attribute x_interface_parameter of axis_s_raw_tdata : signal is "XIL_INTERFACENAME axis_s_raw, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
begin
  axis_m_filtr_tdata(31) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(30) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(29) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(28) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(27) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(26) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(25) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(24) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(23) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(22) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(21) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(20) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(19) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(18) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(17) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(16) <= \^axis_m_filtr_tdata\(30);
  axis_m_filtr_tdata(15 downto 0) <= \^axis_m_filtr_tdata\(15 downto 0);
U0: entity work.block_1_FIR_0_0_FIR
     port map (
      axis_m_filtr_tdata(16) => \^axis_m_filtr_tdata\(30),
      axis_m_filtr_tdata(15 downto 0) => \^axis_m_filtr_tdata\(15 downto 0),
      axis_m_filtr_tlast => axis_m_filtr_tlast,
      axis_m_filtr_tready => axis_m_filtr_tready,
      axis_m_filtr_tvalid => axis_m_filtr_tvalid,
      axis_s_raw_tdata(15 downto 0) => axis_s_raw_tdata(15 downto 0),
      axis_s_raw_tlast => axis_s_raw_tlast,
      axis_s_raw_tready => axis_s_raw_tready,
      axis_s_raw_tvalid => axis_s_raw_tvalid,
      clock => clock
    );
end STRUCTURE;
