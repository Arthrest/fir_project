-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
-- Date        : Mon Dec  3 15:14:46 2018
-- Host        : PC05 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               c:/Users/student/Documents/AK/FIR/fir_project/project_2/project_2.srcs/sources_1/bd/block_1/ip/block_1_FIR_0_0/block_1_FIR_0_0_stub.vhdl
-- Design      : block_1_FIR_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity block_1_FIR_0_0 is
  Port ( 
    clock : in STD_LOGIC;
    axis_s_raw_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axis_s_raw_tready : out STD_LOGIC;
    axis_s_raw_tvalid : in STD_LOGIC;
    axis_s_raw_tlast : in STD_LOGIC;
    axis_m_filtr_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axis_m_filtr_tready : in STD_LOGIC;
    axis_m_filtr_tvalid : out STD_LOGIC;
    axis_m_filtr_tlast : out STD_LOGIC
  );

end block_1_FIR_0_0;

architecture stub of block_1_FIR_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clock,axis_s_raw_tdata[31:0],axis_s_raw_tready,axis_s_raw_tvalid,axis_s_raw_tlast,axis_m_filtr_tdata[31:0],axis_m_filtr_tready,axis_m_filtr_tvalid,axis_m_filtr_tlast";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "FIR,Vivado 2018.2";
begin
end;
