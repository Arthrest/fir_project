// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Mon Dec  3 15:14:46 2018
// Host        : PC05 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ block_1_FIR_0_0_sim_netlist.v
// Design      : block_1_FIR_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_FIR
   (axis_s_raw_tready,
    axis_m_filtr_tdata,
    axis_m_filtr_tvalid,
    axis_m_filtr_tlast,
    axis_m_filtr_tready,
    clock,
    axis_s_raw_tdata,
    axis_s_raw_tvalid,
    axis_s_raw_tlast);
  output axis_s_raw_tready;
  output [16:0]axis_m_filtr_tdata;
  output axis_m_filtr_tvalid;
  output axis_m_filtr_tlast;
  input axis_m_filtr_tready;
  input clock;
  input [15:0]axis_s_raw_tdata;
  input axis_s_raw_tvalid;
  input axis_s_raw_tlast;

  wire SHIFT_RIGHT0;
  wire [16:0]axis_m_filtr_tdata;
  wire axis_m_filtr_tlast;
  wire axis_m_filtr_tready;
  wire axis_m_filtr_tvalid;
  wire [15:0]axis_s_raw_tdata;
  wire axis_s_raw_tlast;
  wire axis_s_raw_tready;
  wire axis_s_raw_tvalid;
  wire clock;
  wire \result[13]_i_2_n_0 ;
  wire \result[13]_i_3_n_0 ;
  wire \result[13]_i_4_n_0 ;
  wire \result[17]_i_2_n_0 ;
  wire \result[17]_i_3_n_0 ;
  wire \result[17]_i_4_n_0 ;
  wire \result[17]_i_5_n_0 ;
  wire \result[21]_i_2_n_0 ;
  wire \result[21]_i_3_n_0 ;
  wire \result[21]_i_4_n_0 ;
  wire \result[21]_i_5_n_0 ;
  wire \result[25]_i_2_n_0 ;
  wire \result[25]_i_3_n_0 ;
  wire \result[25]_i_4_n_0 ;
  wire \result[25]_i_5_n_0 ;
  wire \result[29]_i_2_n_0 ;
  wire \result[29]_i_3_n_0 ;
  wire \result[29]_i_4_n_0 ;
  wire [31:15]result_final0;
  wire result_final0_carry__0_i_1_n_0;
  wire result_final0_carry__0_i_2_n_0;
  wire result_final0_carry__0_i_3_n_0;
  wire result_final0_carry__0_i_4_n_0;
  wire result_final0_carry__0_n_0;
  wire result_final0_carry__0_n_1;
  wire result_final0_carry__0_n_2;
  wire result_final0_carry__0_n_3;
  wire result_final0_carry__1_i_1_n_0;
  wire result_final0_carry__1_i_2_n_0;
  wire result_final0_carry__1_i_3_n_0;
  wire result_final0_carry__1_i_4_n_0;
  wire result_final0_carry__1_n_0;
  wire result_final0_carry__1_n_1;
  wire result_final0_carry__1_n_2;
  wire result_final0_carry__1_n_3;
  wire result_final0_carry__2_i_1_n_0;
  wire result_final0_carry__2_i_2_n_0;
  wire result_final0_carry__2_i_3_n_0;
  wire result_final0_carry__2_i_4_n_0;
  wire result_final0_carry__2_n_0;
  wire result_final0_carry__2_n_1;
  wire result_final0_carry__2_n_2;
  wire result_final0_carry__2_n_3;
  wire result_final0_carry__3_i_1_n_0;
  wire result_final0_carry__3_i_2_n_0;
  wire result_final0_carry__3_i_3_n_0;
  wire result_final0_carry__3_n_2;
  wire result_final0_carry__3_n_3;
  wire result_final0_carry_i_1_n_0;
  wire result_final0_carry_i_2_n_0;
  wire result_final0_carry_i_3_n_0;
  wire result_final0_carry_i_4_n_0;
  wire result_final0_carry_n_0;
  wire result_final0_carry_n_1;
  wire result_final0_carry_n_2;
  wire result_final0_carry_n_3;
  wire \result_final_reg_n_0_[15] ;
  wire \result_final_reg_n_0_[16] ;
  wire \result_final_reg_n_0_[17] ;
  wire \result_final_reg_n_0_[18] ;
  wire \result_final_reg_n_0_[19] ;
  wire \result_final_reg_n_0_[20] ;
  wire \result_final_reg_n_0_[21] ;
  wire \result_final_reg_n_0_[22] ;
  wire \result_final_reg_n_0_[23] ;
  wire \result_final_reg_n_0_[24] ;
  wire \result_final_reg_n_0_[25] ;
  wire \result_final_reg_n_0_[26] ;
  wire \result_final_reg_n_0_[27] ;
  wire \result_final_reg_n_0_[28] ;
  wire \result_final_reg_n_0_[29] ;
  wire \result_final_reg_n_0_[30] ;
  wire [31:13]result_guard;
  wire [31:13]result_reg;
  wire \result_reg[13]_i_1_n_0 ;
  wire \result_reg[13]_i_1_n_1 ;
  wire \result_reg[13]_i_1_n_2 ;
  wire \result_reg[13]_i_1_n_3 ;
  wire \result_reg[13]_i_1_n_4 ;
  wire \result_reg[13]_i_1_n_5 ;
  wire \result_reg[13]_i_1_n_6 ;
  wire \result_reg[13]_i_1_n_7 ;
  wire \result_reg[17]_i_1_n_0 ;
  wire \result_reg[17]_i_1_n_1 ;
  wire \result_reg[17]_i_1_n_2 ;
  wire \result_reg[17]_i_1_n_3 ;
  wire \result_reg[17]_i_1_n_4 ;
  wire \result_reg[17]_i_1_n_5 ;
  wire \result_reg[17]_i_1_n_6 ;
  wire \result_reg[17]_i_1_n_7 ;
  wire \result_reg[21]_i_1_n_0 ;
  wire \result_reg[21]_i_1_n_1 ;
  wire \result_reg[21]_i_1_n_2 ;
  wire \result_reg[21]_i_1_n_3 ;
  wire \result_reg[21]_i_1_n_4 ;
  wire \result_reg[21]_i_1_n_5 ;
  wire \result_reg[21]_i_1_n_6 ;
  wire \result_reg[21]_i_1_n_7 ;
  wire \result_reg[25]_i_1_n_0 ;
  wire \result_reg[25]_i_1_n_1 ;
  wire \result_reg[25]_i_1_n_2 ;
  wire \result_reg[25]_i_1_n_3 ;
  wire \result_reg[25]_i_1_n_4 ;
  wire \result_reg[25]_i_1_n_5 ;
  wire \result_reg[25]_i_1_n_6 ;
  wire \result_reg[25]_i_1_n_7 ;
  wire \result_reg[29]_i_1_n_2 ;
  wire \result_reg[29]_i_1_n_3 ;
  wire \result_reg[29]_i_1_n_5 ;
  wire \result_reg[29]_i_1_n_6 ;
  wire \result_reg[29]_i_1_n_7 ;
  wire \tab_reg_n_0_[0][0] ;
  wire \tab_reg_n_0_[0][10] ;
  wire \tab_reg_n_0_[0][11] ;
  wire \tab_reg_n_0_[0][12] ;
  wire \tab_reg_n_0_[0][13] ;
  wire \tab_reg_n_0_[0][14] ;
  wire \tab_reg_n_0_[0][15] ;
  wire \tab_reg_n_0_[0][1] ;
  wire \tab_reg_n_0_[0][2] ;
  wire \tab_reg_n_0_[0][3] ;
  wire \tab_reg_n_0_[0][4] ;
  wire \tab_reg_n_0_[0][5] ;
  wire \tab_reg_n_0_[0][6] ;
  wire \tab_reg_n_0_[0][7] ;
  wire \tab_reg_n_0_[0][8] ;
  wire \tab_reg_n_0_[0][9] ;
  wire \tab_reg_n_0_[1][0] ;
  wire \tab_reg_n_0_[1][10] ;
  wire \tab_reg_n_0_[1][11] ;
  wire \tab_reg_n_0_[1][12] ;
  wire \tab_reg_n_0_[1][13] ;
  wire \tab_reg_n_0_[1][14] ;
  wire \tab_reg_n_0_[1][15] ;
  wire \tab_reg_n_0_[1][1] ;
  wire \tab_reg_n_0_[1][2] ;
  wire \tab_reg_n_0_[1][3] ;
  wire \tab_reg_n_0_[1][4] ;
  wire \tab_reg_n_0_[1][5] ;
  wire \tab_reg_n_0_[1][6] ;
  wire \tab_reg_n_0_[1][7] ;
  wire \tab_reg_n_0_[1][8] ;
  wire \tab_reg_n_0_[1][9] ;
  wire [1:0]NLW_result_final0_carry_O_UNCONNECTED;
  wire [3:2]NLW_result_final0_carry__3_CO_UNCONNECTED;
  wire [3:3]NLW_result_final0_carry__3_O_UNCONNECTED;
  wire [3:2]\NLW_result_reg[29]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_result_reg[29]_i_1_O_UNCONNECTED ;

  FDRE \axis_m_filtr_tdata_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[15] ),
        .Q(axis_m_filtr_tdata[0]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[10] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[25] ),
        .Q(axis_m_filtr_tdata[10]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[11] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[26] ),
        .Q(axis_m_filtr_tdata[11]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[12] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[27] ),
        .Q(axis_m_filtr_tdata[12]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[13] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[28] ),
        .Q(axis_m_filtr_tdata[13]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[14] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[29] ),
        .Q(axis_m_filtr_tdata[14]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[15] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[30] ),
        .Q(axis_m_filtr_tdata[15]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[16] ),
        .Q(axis_m_filtr_tdata[1]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[17] ),
        .Q(axis_m_filtr_tdata[2]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[31] 
       (.C(clock),
        .CE(1'b1),
        .D(SHIFT_RIGHT0),
        .Q(axis_m_filtr_tdata[16]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[3] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[18] ),
        .Q(axis_m_filtr_tdata[3]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[4] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[19] ),
        .Q(axis_m_filtr_tdata[4]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[5] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[20] ),
        .Q(axis_m_filtr_tdata[5]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[6] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[21] ),
        .Q(axis_m_filtr_tdata[6]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[7] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[22] ),
        .Q(axis_m_filtr_tdata[7]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[8] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[23] ),
        .Q(axis_m_filtr_tdata[8]),
        .R(1'b0));
  FDRE \axis_m_filtr_tdata_reg[9] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_final_reg_n_0_[24] ),
        .Q(axis_m_filtr_tdata[9]),
        .R(1'b0));
  FDRE axis_m_filtr_tlast_reg
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tlast),
        .Q(axis_m_filtr_tlast),
        .R(1'b0));
  FDRE axis_m_filtr_tvalid_reg
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tvalid),
        .Q(axis_m_filtr_tvalid),
        .R(1'b0));
  FDRE axis_s_raw_tready_reg
       (.C(clock),
        .CE(1'b1),
        .D(axis_m_filtr_tready),
        .Q(axis_s_raw_tready),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \result[13]_i_2 
       (.I0(\tab_reg_n_0_[1][2] ),
        .I1(result_reg[16]),
        .O(\result[13]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[13]_i_3 
       (.I0(\tab_reg_n_0_[1][1] ),
        .I1(result_reg[15]),
        .O(\result[13]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[13]_i_4 
       (.I0(\tab_reg_n_0_[1][0] ),
        .I1(result_reg[14]),
        .O(\result[13]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[17]_i_2 
       (.I0(\tab_reg_n_0_[1][6] ),
        .I1(result_reg[20]),
        .O(\result[17]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[17]_i_3 
       (.I0(\tab_reg_n_0_[1][5] ),
        .I1(result_reg[19]),
        .O(\result[17]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[17]_i_4 
       (.I0(\tab_reg_n_0_[1][4] ),
        .I1(result_reg[18]),
        .O(\result[17]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[17]_i_5 
       (.I0(\tab_reg_n_0_[1][3] ),
        .I1(result_reg[17]),
        .O(\result[17]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[21]_i_2 
       (.I0(\tab_reg_n_0_[1][10] ),
        .I1(result_reg[24]),
        .O(\result[21]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[21]_i_3 
       (.I0(\tab_reg_n_0_[1][9] ),
        .I1(result_reg[23]),
        .O(\result[21]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[21]_i_4 
       (.I0(\tab_reg_n_0_[1][8] ),
        .I1(result_reg[22]),
        .O(\result[21]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[21]_i_5 
       (.I0(\tab_reg_n_0_[1][7] ),
        .I1(result_reg[21]),
        .O(\result[21]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[25]_i_2 
       (.I0(\tab_reg_n_0_[1][14] ),
        .I1(result_reg[28]),
        .O(\result[25]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[25]_i_3 
       (.I0(\tab_reg_n_0_[1][13] ),
        .I1(result_reg[27]),
        .O(\result[25]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[25]_i_4 
       (.I0(\tab_reg_n_0_[1][12] ),
        .I1(result_reg[26]),
        .O(\result[25]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[25]_i_5 
       (.I0(\tab_reg_n_0_[1][11] ),
        .I1(result_reg[25]),
        .O(\result[25]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[29]_i_2 
       (.I0(\tab_reg_n_0_[1][15] ),
        .I1(result_reg[31]),
        .O(\result[29]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[29]_i_3 
       (.I0(\tab_reg_n_0_[1][15] ),
        .I1(result_reg[30]),
        .O(\result[29]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \result[29]_i_4 
       (.I0(\tab_reg_n_0_[1][15] ),
        .I1(result_reg[29]),
        .O(\result[29]_i_4_n_0 ));
  CARRY4 result_final0_carry
       (.CI(1'b0),
        .CO({result_final0_carry_n_0,result_final0_carry_n_1,result_final0_carry_n_2,result_final0_carry_n_3}),
        .CYINIT(1'b1),
        .DI(result_reg[16:13]),
        .O({result_final0[16:15],NLW_result_final0_carry_O_UNCONNECTED[1:0]}),
        .S({result_final0_carry_i_1_n_0,result_final0_carry_i_2_n_0,result_final0_carry_i_3_n_0,result_final0_carry_i_4_n_0}));
  CARRY4 result_final0_carry__0
       (.CI(result_final0_carry_n_0),
        .CO({result_final0_carry__0_n_0,result_final0_carry__0_n_1,result_final0_carry__0_n_2,result_final0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(result_reg[20:17]),
        .O(result_final0[20:17]),
        .S({result_final0_carry__0_i_1_n_0,result_final0_carry__0_i_2_n_0,result_final0_carry__0_i_3_n_0,result_final0_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__0_i_1
       (.I0(result_reg[20]),
        .I1(result_guard[20]),
        .O(result_final0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__0_i_2
       (.I0(result_reg[19]),
        .I1(result_guard[19]),
        .O(result_final0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__0_i_3
       (.I0(result_reg[18]),
        .I1(result_guard[18]),
        .O(result_final0_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__0_i_4
       (.I0(result_reg[17]),
        .I1(result_guard[17]),
        .O(result_final0_carry__0_i_4_n_0));
  CARRY4 result_final0_carry__1
       (.CI(result_final0_carry__0_n_0),
        .CO({result_final0_carry__1_n_0,result_final0_carry__1_n_1,result_final0_carry__1_n_2,result_final0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(result_reg[24:21]),
        .O(result_final0[24:21]),
        .S({result_final0_carry__1_i_1_n_0,result_final0_carry__1_i_2_n_0,result_final0_carry__1_i_3_n_0,result_final0_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__1_i_1
       (.I0(result_reg[24]),
        .I1(result_guard[24]),
        .O(result_final0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__1_i_2
       (.I0(result_reg[23]),
        .I1(result_guard[23]),
        .O(result_final0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__1_i_3
       (.I0(result_reg[22]),
        .I1(result_guard[22]),
        .O(result_final0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__1_i_4
       (.I0(result_reg[21]),
        .I1(result_guard[21]),
        .O(result_final0_carry__1_i_4_n_0));
  CARRY4 result_final0_carry__2
       (.CI(result_final0_carry__1_n_0),
        .CO({result_final0_carry__2_n_0,result_final0_carry__2_n_1,result_final0_carry__2_n_2,result_final0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(result_reg[28:25]),
        .O(result_final0[28:25]),
        .S({result_final0_carry__2_i_1_n_0,result_final0_carry__2_i_2_n_0,result_final0_carry__2_i_3_n_0,result_final0_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__2_i_1
       (.I0(result_reg[28]),
        .I1(result_guard[28]),
        .O(result_final0_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__2_i_2
       (.I0(result_reg[27]),
        .I1(result_guard[27]),
        .O(result_final0_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__2_i_3
       (.I0(result_reg[26]),
        .I1(result_guard[26]),
        .O(result_final0_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__2_i_4
       (.I0(result_reg[25]),
        .I1(result_guard[25]),
        .O(result_final0_carry__2_i_4_n_0));
  CARRY4 result_final0_carry__3
       (.CI(result_final0_carry__2_n_0),
        .CO({NLW_result_final0_carry__3_CO_UNCONNECTED[3:2],result_final0_carry__3_n_2,result_final0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,result_reg[30:29]}),
        .O({NLW_result_final0_carry__3_O_UNCONNECTED[3],result_final0[31:29]}),
        .S({1'b0,result_final0_carry__3_i_1_n_0,result_final0_carry__3_i_2_n_0,result_final0_carry__3_i_3_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__3_i_1
       (.I0(result_reg[31]),
        .I1(result_guard[31]),
        .O(result_final0_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__3_i_2
       (.I0(result_reg[30]),
        .I1(result_guard[30]),
        .O(result_final0_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry__3_i_3
       (.I0(result_reg[29]),
        .I1(result_guard[29]),
        .O(result_final0_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry_i_1
       (.I0(result_reg[16]),
        .I1(result_guard[16]),
        .O(result_final0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry_i_2
       (.I0(result_reg[15]),
        .I1(result_guard[15]),
        .O(result_final0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry_i_3
       (.I0(result_reg[14]),
        .I1(result_guard[14]),
        .O(result_final0_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    result_final0_carry_i_4
       (.I0(result_reg[13]),
        .I1(result_guard[13]),
        .O(result_final0_carry_i_4_n_0));
  FDRE \result_final_reg[15] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[15]),
        .Q(\result_final_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \result_final_reg[16] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[16]),
        .Q(\result_final_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \result_final_reg[17] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[17]),
        .Q(\result_final_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \result_final_reg[18] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[18]),
        .Q(\result_final_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \result_final_reg[19] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[19]),
        .Q(\result_final_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \result_final_reg[20] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[20]),
        .Q(\result_final_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \result_final_reg[21] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[21]),
        .Q(\result_final_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \result_final_reg[22] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[22]),
        .Q(\result_final_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \result_final_reg[23] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[23]),
        .Q(\result_final_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \result_final_reg[24] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[24]),
        .Q(\result_final_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \result_final_reg[25] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[25]),
        .Q(\result_final_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \result_final_reg[26] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[26]),
        .Q(\result_final_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \result_final_reg[27] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[27]),
        .Q(\result_final_reg_n_0_[27] ),
        .R(1'b0));
  FDRE \result_final_reg[28] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[28]),
        .Q(\result_final_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \result_final_reg[29] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[29]),
        .Q(\result_final_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \result_final_reg[30] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[30]),
        .Q(\result_final_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \result_final_reg[31] 
       (.C(clock),
        .CE(1'b1),
        .D(result_final0[31]),
        .Q(SHIFT_RIGHT0),
        .R(1'b0));
  FDRE \result_guard_reg[13] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[13]),
        .Q(result_guard[13]),
        .R(1'b0));
  FDRE \result_guard_reg[14] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[14]),
        .Q(result_guard[14]),
        .R(1'b0));
  FDRE \result_guard_reg[15] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[15]),
        .Q(result_guard[15]),
        .R(1'b0));
  FDRE \result_guard_reg[16] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[16]),
        .Q(result_guard[16]),
        .R(1'b0));
  FDRE \result_guard_reg[17] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[17]),
        .Q(result_guard[17]),
        .R(1'b0));
  FDRE \result_guard_reg[18] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[18]),
        .Q(result_guard[18]),
        .R(1'b0));
  FDRE \result_guard_reg[19] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[19]),
        .Q(result_guard[19]),
        .R(1'b0));
  FDRE \result_guard_reg[20] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[20]),
        .Q(result_guard[20]),
        .R(1'b0));
  FDRE \result_guard_reg[21] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[21]),
        .Q(result_guard[21]),
        .R(1'b0));
  FDRE \result_guard_reg[22] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[22]),
        .Q(result_guard[22]),
        .R(1'b0));
  FDRE \result_guard_reg[23] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[23]),
        .Q(result_guard[23]),
        .R(1'b0));
  FDRE \result_guard_reg[24] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[24]),
        .Q(result_guard[24]),
        .R(1'b0));
  FDRE \result_guard_reg[25] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[25]),
        .Q(result_guard[25]),
        .R(1'b0));
  FDRE \result_guard_reg[26] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[26]),
        .Q(result_guard[26]),
        .R(1'b0));
  FDRE \result_guard_reg[27] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[27]),
        .Q(result_guard[27]),
        .R(1'b0));
  FDRE \result_guard_reg[28] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[28]),
        .Q(result_guard[28]),
        .R(1'b0));
  FDRE \result_guard_reg[29] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[29]),
        .Q(result_guard[29]),
        .R(1'b0));
  FDRE \result_guard_reg[30] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[30]),
        .Q(result_guard[30]),
        .R(1'b0));
  FDRE \result_guard_reg[31] 
       (.C(clock),
        .CE(1'b1),
        .D(result_reg[31]),
        .Q(result_guard[31]),
        .R(1'b0));
  FDRE \result_reg[13] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[13]_i_1_n_7 ),
        .Q(result_reg[13]),
        .R(1'b0));
  CARRY4 \result_reg[13]_i_1 
       (.CI(1'b0),
        .CO({\result_reg[13]_i_1_n_0 ,\result_reg[13]_i_1_n_1 ,\result_reg[13]_i_1_n_2 ,\result_reg[13]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\tab_reg_n_0_[1][2] ,\tab_reg_n_0_[1][1] ,\tab_reg_n_0_[1][0] ,1'b0}),
        .O({\result_reg[13]_i_1_n_4 ,\result_reg[13]_i_1_n_5 ,\result_reg[13]_i_1_n_6 ,\result_reg[13]_i_1_n_7 }),
        .S({\result[13]_i_2_n_0 ,\result[13]_i_3_n_0 ,\result[13]_i_4_n_0 ,result_reg[13]}));
  FDRE \result_reg[14] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[13]_i_1_n_6 ),
        .Q(result_reg[14]),
        .R(1'b0));
  FDRE \result_reg[15] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[13]_i_1_n_5 ),
        .Q(result_reg[15]),
        .R(1'b0));
  FDRE \result_reg[16] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[13]_i_1_n_4 ),
        .Q(result_reg[16]),
        .R(1'b0));
  FDRE \result_reg[17] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[17]_i_1_n_7 ),
        .Q(result_reg[17]),
        .R(1'b0));
  CARRY4 \result_reg[17]_i_1 
       (.CI(\result_reg[13]_i_1_n_0 ),
        .CO({\result_reg[17]_i_1_n_0 ,\result_reg[17]_i_1_n_1 ,\result_reg[17]_i_1_n_2 ,\result_reg[17]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\tab_reg_n_0_[1][6] ,\tab_reg_n_0_[1][5] ,\tab_reg_n_0_[1][4] ,\tab_reg_n_0_[1][3] }),
        .O({\result_reg[17]_i_1_n_4 ,\result_reg[17]_i_1_n_5 ,\result_reg[17]_i_1_n_6 ,\result_reg[17]_i_1_n_7 }),
        .S({\result[17]_i_2_n_0 ,\result[17]_i_3_n_0 ,\result[17]_i_4_n_0 ,\result[17]_i_5_n_0 }));
  FDRE \result_reg[18] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[17]_i_1_n_6 ),
        .Q(result_reg[18]),
        .R(1'b0));
  FDRE \result_reg[19] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[17]_i_1_n_5 ),
        .Q(result_reg[19]),
        .R(1'b0));
  FDRE \result_reg[20] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[17]_i_1_n_4 ),
        .Q(result_reg[20]),
        .R(1'b0));
  FDRE \result_reg[21] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[21]_i_1_n_7 ),
        .Q(result_reg[21]),
        .R(1'b0));
  CARRY4 \result_reg[21]_i_1 
       (.CI(\result_reg[17]_i_1_n_0 ),
        .CO({\result_reg[21]_i_1_n_0 ,\result_reg[21]_i_1_n_1 ,\result_reg[21]_i_1_n_2 ,\result_reg[21]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\tab_reg_n_0_[1][10] ,\tab_reg_n_0_[1][9] ,\tab_reg_n_0_[1][8] ,\tab_reg_n_0_[1][7] }),
        .O({\result_reg[21]_i_1_n_4 ,\result_reg[21]_i_1_n_5 ,\result_reg[21]_i_1_n_6 ,\result_reg[21]_i_1_n_7 }),
        .S({\result[21]_i_2_n_0 ,\result[21]_i_3_n_0 ,\result[21]_i_4_n_0 ,\result[21]_i_5_n_0 }));
  FDRE \result_reg[22] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[21]_i_1_n_6 ),
        .Q(result_reg[22]),
        .R(1'b0));
  FDRE \result_reg[23] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[21]_i_1_n_5 ),
        .Q(result_reg[23]),
        .R(1'b0));
  FDRE \result_reg[24] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[21]_i_1_n_4 ),
        .Q(result_reg[24]),
        .R(1'b0));
  FDRE \result_reg[25] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[25]_i_1_n_7 ),
        .Q(result_reg[25]),
        .R(1'b0));
  CARRY4 \result_reg[25]_i_1 
       (.CI(\result_reg[21]_i_1_n_0 ),
        .CO({\result_reg[25]_i_1_n_0 ,\result_reg[25]_i_1_n_1 ,\result_reg[25]_i_1_n_2 ,\result_reg[25]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\tab_reg_n_0_[1][14] ,\tab_reg_n_0_[1][13] ,\tab_reg_n_0_[1][12] ,\tab_reg_n_0_[1][11] }),
        .O({\result_reg[25]_i_1_n_4 ,\result_reg[25]_i_1_n_5 ,\result_reg[25]_i_1_n_6 ,\result_reg[25]_i_1_n_7 }),
        .S({\result[25]_i_2_n_0 ,\result[25]_i_3_n_0 ,\result[25]_i_4_n_0 ,\result[25]_i_5_n_0 }));
  FDRE \result_reg[26] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[25]_i_1_n_6 ),
        .Q(result_reg[26]),
        .R(1'b0));
  FDRE \result_reg[27] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[25]_i_1_n_5 ),
        .Q(result_reg[27]),
        .R(1'b0));
  FDRE \result_reg[28] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[25]_i_1_n_4 ),
        .Q(result_reg[28]),
        .R(1'b0));
  FDRE \result_reg[29] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[29]_i_1_n_7 ),
        .Q(result_reg[29]),
        .R(1'b0));
  CARRY4 \result_reg[29]_i_1 
       (.CI(\result_reg[25]_i_1_n_0 ),
        .CO({\NLW_result_reg[29]_i_1_CO_UNCONNECTED [3:2],\result_reg[29]_i_1_n_2 ,\result_reg[29]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\tab_reg_n_0_[1][15] ,\tab_reg_n_0_[1][15] }),
        .O({\NLW_result_reg[29]_i_1_O_UNCONNECTED [3],\result_reg[29]_i_1_n_5 ,\result_reg[29]_i_1_n_6 ,\result_reg[29]_i_1_n_7 }),
        .S({1'b0,\result[29]_i_2_n_0 ,\result[29]_i_3_n_0 ,\result[29]_i_4_n_0 }));
  FDRE \result_reg[30] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[29]_i_1_n_6 ),
        .Q(result_reg[30]),
        .R(1'b0));
  FDRE \result_reg[31] 
       (.C(clock),
        .CE(1'b1),
        .D(\result_reg[29]_i_1_n_5 ),
        .Q(result_reg[31]),
        .R(1'b0));
  FDRE \tab_reg[0][0] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[0]),
        .Q(\tab_reg_n_0_[0][0] ),
        .R(1'b0));
  FDRE \tab_reg[0][10] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[10]),
        .Q(\tab_reg_n_0_[0][10] ),
        .R(1'b0));
  FDRE \tab_reg[0][11] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[11]),
        .Q(\tab_reg_n_0_[0][11] ),
        .R(1'b0));
  FDRE \tab_reg[0][12] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[12]),
        .Q(\tab_reg_n_0_[0][12] ),
        .R(1'b0));
  FDRE \tab_reg[0][13] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[13]),
        .Q(\tab_reg_n_0_[0][13] ),
        .R(1'b0));
  FDRE \tab_reg[0][14] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[14]),
        .Q(\tab_reg_n_0_[0][14] ),
        .R(1'b0));
  FDRE \tab_reg[0][15] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[15]),
        .Q(\tab_reg_n_0_[0][15] ),
        .R(1'b0));
  FDRE \tab_reg[0][1] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[1]),
        .Q(\tab_reg_n_0_[0][1] ),
        .R(1'b0));
  FDRE \tab_reg[0][2] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[2]),
        .Q(\tab_reg_n_0_[0][2] ),
        .R(1'b0));
  FDRE \tab_reg[0][3] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[3]),
        .Q(\tab_reg_n_0_[0][3] ),
        .R(1'b0));
  FDRE \tab_reg[0][4] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[4]),
        .Q(\tab_reg_n_0_[0][4] ),
        .R(1'b0));
  FDRE \tab_reg[0][5] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[5]),
        .Q(\tab_reg_n_0_[0][5] ),
        .R(1'b0));
  FDRE \tab_reg[0][6] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[6]),
        .Q(\tab_reg_n_0_[0][6] ),
        .R(1'b0));
  FDRE \tab_reg[0][7] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[7]),
        .Q(\tab_reg_n_0_[0][7] ),
        .R(1'b0));
  FDRE \tab_reg[0][8] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[8]),
        .Q(\tab_reg_n_0_[0][8] ),
        .R(1'b0));
  FDRE \tab_reg[0][9] 
       (.C(clock),
        .CE(1'b1),
        .D(axis_s_raw_tdata[9]),
        .Q(\tab_reg_n_0_[0][9] ),
        .R(1'b0));
  FDRE \tab_reg[1][0] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][0] ),
        .Q(\tab_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \tab_reg[1][10] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][10] ),
        .Q(\tab_reg_n_0_[1][10] ),
        .R(1'b0));
  FDRE \tab_reg[1][11] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][11] ),
        .Q(\tab_reg_n_0_[1][11] ),
        .R(1'b0));
  FDRE \tab_reg[1][12] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][12] ),
        .Q(\tab_reg_n_0_[1][12] ),
        .R(1'b0));
  FDRE \tab_reg[1][13] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][13] ),
        .Q(\tab_reg_n_0_[1][13] ),
        .R(1'b0));
  FDRE \tab_reg[1][14] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][14] ),
        .Q(\tab_reg_n_0_[1][14] ),
        .R(1'b0));
  FDRE \tab_reg[1][15] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][15] ),
        .Q(\tab_reg_n_0_[1][15] ),
        .R(1'b0));
  FDRE \tab_reg[1][1] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][1] ),
        .Q(\tab_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \tab_reg[1][2] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][2] ),
        .Q(\tab_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \tab_reg[1][3] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][3] ),
        .Q(\tab_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \tab_reg[1][4] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][4] ),
        .Q(\tab_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \tab_reg[1][5] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][5] ),
        .Q(\tab_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \tab_reg[1][6] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][6] ),
        .Q(\tab_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \tab_reg[1][7] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][7] ),
        .Q(\tab_reg_n_0_[1][7] ),
        .R(1'b0));
  FDRE \tab_reg[1][8] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][8] ),
        .Q(\tab_reg_n_0_[1][8] ),
        .R(1'b0));
  FDRE \tab_reg[1][9] 
       (.C(clock),
        .CE(1'b1),
        .D(\tab_reg_n_0_[0][9] ),
        .Q(\tab_reg_n_0_[1][9] ),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "block_1_FIR_0_0,FIR,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "FIR,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clock,
    axis_s_raw_tdata,
    axis_s_raw_tready,
    axis_s_raw_tvalid,
    axis_s_raw_tlast,
    axis_m_filtr_tdata,
    axis_m_filtr_tready,
    axis_m_filtr_tvalid,
    axis_m_filtr_tlast);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clock CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clock, ASSOCIATED_BUSIF axis_m_filtr:axis_s_raw, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_1_processing_system7_0_0_FCLK_CLK0" *) input clock;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_s_raw TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME axis_s_raw, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input [31:0]axis_s_raw_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_s_raw TREADY" *) output axis_s_raw_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_s_raw TVALID" *) input axis_s_raw_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_s_raw TLAST" *) input axis_s_raw_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_m_filtr TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME axis_m_filtr, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) output [31:0]axis_m_filtr_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_m_filtr TREADY" *) input axis_m_filtr_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_m_filtr TVALID" *) output axis_m_filtr_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 axis_m_filtr TLAST" *) output axis_m_filtr_tlast;

  wire [30:0]\^axis_m_filtr_tdata ;
  wire axis_m_filtr_tlast;
  wire axis_m_filtr_tready;
  wire axis_m_filtr_tvalid;
  wire [31:0]axis_s_raw_tdata;
  wire axis_s_raw_tlast;
  wire axis_s_raw_tready;
  wire axis_s_raw_tvalid;
  wire clock;

  assign axis_m_filtr_tdata[31] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[30] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[29] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[28] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[27] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[26] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[25] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[24] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[23] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[22] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[21] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[20] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[19] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[18] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[17] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[16] = \^axis_m_filtr_tdata [30];
  assign axis_m_filtr_tdata[15:0] = \^axis_m_filtr_tdata [15:0];
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_FIR U0
       (.axis_m_filtr_tdata({\^axis_m_filtr_tdata [30],\^axis_m_filtr_tdata [15:0]}),
        .axis_m_filtr_tlast(axis_m_filtr_tlast),
        .axis_m_filtr_tready(axis_m_filtr_tready),
        .axis_m_filtr_tvalid(axis_m_filtr_tvalid),
        .axis_s_raw_tdata(axis_s_raw_tdata[15:0]),
        .axis_s_raw_tlast(axis_s_raw_tlast),
        .axis_s_raw_tready(axis_s_raw_tready),
        .axis_s_raw_tvalid(axis_s_raw_tvalid),
        .clock(clock));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
