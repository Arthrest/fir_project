-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
-- Date        : Mon Dec  3 15:14:46 2018
-- Host        : PC05 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ block_1_FIR_0_0_stub.vhdl
-- Design      : block_1_FIR_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clock : in STD_LOGIC;
    axis_s_raw_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axis_s_raw_tready : out STD_LOGIC;
    axis_s_raw_tvalid : in STD_LOGIC;
    axis_s_raw_tlast : in STD_LOGIC;
    axis_m_filtr_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axis_m_filtr_tready : in STD_LOGIC;
    axis_m_filtr_tvalid : out STD_LOGIC;
    axis_m_filtr_tlast : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clock,axis_s_raw_tdata[31:0],axis_s_raw_tready,axis_s_raw_tvalid,axis_s_raw_tlast,axis_m_filtr_tdata[31:0],axis_m_filtr_tready,axis_m_filtr_tvalid,axis_m_filtr_tlast";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "FIR,Vivado 2018.2";
begin
end;
