import threading

import serial
from serial import SerialException


class SerialComThread(threading.Thread):
    def __init__(self, exitEvent, comPort, baudRate):
        threading.Thread.__init__(self)
        self.exitEvent = exitEvent
        self.comPort = comPort
        self.baud = baudRate

    def run(self):
        try:
            with serial.Serial(self.comPort, self.baud, timeout=1) as ser:
                while self.exitEvent.is_set():
                    ser.write("ok\n")
        except SerialException:
            print("Mamusiu nie działa :<< ")


# użycie
exitThreadsRunFlag = threading.Event()
SerialComThread(exitThreadsRunFlag, "COM5", 9600)